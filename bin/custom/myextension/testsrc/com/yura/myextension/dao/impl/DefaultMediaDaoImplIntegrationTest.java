package com.yura.myextension.dao.impl;

import com.yura.myextension.dao.MediaDao;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;

public class DefaultMediaDaoImplIntegrationTest extends ServicelayerTransactionalTest {
    private static final String PRODUCT_CODE = "Best Product";
    private static final String PICTURE_CODE = "Picture";
    private static final String PICTURE_DESCRIPTION = "Best Picture";
    private static final String CATALOG_VERSION = "Staged";
    private static final String CATALOG_ID = "Default";
    private static final String WRONG_PRODUCT_CODE = "Wrong Code";

    private CatalogVersionModel catalogVersionModel;

    @Resource(name = "defaultMediaDaoImpl")
    private MediaDao mediaDao;

    @Resource
    private ModelService modelService;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void setup() {
        catalogVersionModel = getCatalogVersionModel(getCatalogModel());
        saveProductModel();
    }

    @Test
    public void shouldReturnImageDescriptionForProductCodeAndCatalogVersion() {
        String actual = mediaDao.findDescriptionByProductCodeAndCatalogVersion(PRODUCT_CODE, catalogVersionModel);

        assertEquals(PICTURE_DESCRIPTION, actual);
    }

    @Test
    public void shouldThrowModelNotFoundException() {
        exception.expect(ModelNotFoundException.class);
        mediaDao.findDescriptionByProductCodeAndCatalogVersion(WRONG_PRODUCT_CODE, catalogVersionModel);
    }

    private void saveProductModel() {
        ProductModel productModel = new ProductModel();
        productModel.setCode(PRODUCT_CODE);
        productModel.setCatalogVersion(catalogVersionModel);
        productModel.setPicture(getMediaModel());
        modelService.save(productModel);
    }

    private MediaModel getMediaModel() {
        MediaModel mediaModel = new MediaModel();
        mediaModel.setCode(PICTURE_CODE);
        mediaModel.setDescription(PICTURE_DESCRIPTION);
        mediaModel.setCatalogVersion(catalogVersionModel);
        modelService.save(mediaModel);
        return mediaModel;
    }

    private CatalogVersionModel getCatalogVersionModel(CatalogModel catalogModel) {
        CatalogVersionModel catalogVersionModel = new CatalogVersionModel();
        catalogVersionModel.setCatalog(catalogModel);
        catalogVersionModel.setVersion(CATALOG_VERSION);
        modelService.save(catalogVersionModel);

        return catalogVersionModel;
    }

    private CatalogModel getCatalogModel() {
        CatalogModel catalogModel = new CatalogModel();
        catalogModel.setId(CATALOG_ID);
        modelService.save(catalogModel);

        return catalogModel;
    }
}
