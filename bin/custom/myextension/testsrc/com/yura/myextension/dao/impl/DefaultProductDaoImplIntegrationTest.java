package com.yura.myextension.dao.impl;

import com.yura.myextension.dao.ProductDao;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.annotation.Resource;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class DefaultProductDaoImplIntegrationTest extends ServicelayerTransactionalTest {
    private static final String PRODUCT_CODE = "12";
    private static final String CATALOG_VERSION = "staged";
    public static final int OFFLINE_DATE_FROM_NOW = 3;
    public static final int DAYS = 5;

    private CatalogVersionModel catalogVersionModel;

    @Resource(name = "defaultProductDaoImpl")
    private ProductDao productDao;

    @Resource
    private ModelService modelService;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setup() {
        CatalogModel catalogModel = new CatalogModel();
        catalogModel.setId("100");
        modelService.save(catalogModel);

        catalogVersionModel = new CatalogVersionModel();
        catalogVersionModel.setCatalog(catalogModel);
        catalogVersionModel.setVersion(CATALOG_VERSION);
        modelService.save(catalogVersionModel);
    }

    @Test
    public void shouldReturnProductModelForCodeAndVersion() {
        ProductModel productModel = getProductModel();
        modelService.save(productModel);

        ProductModel actual = productDao.findByCodeAndCatalogVersion(PRODUCT_CODE, catalogVersionModel);

        assertEquals(productModel, actual);
    }

    @Test
    public void shouldThrowModelNotFoundException() {
        expectedException.expect(ModelNotFoundException.class);

        productDao.findByCodeAndCatalogVersion(PRODUCT_CODE, catalogVersionModel);
    }

    @Test
    public void shouldReturnListOfProducts() {
        ProductModel productModel = getProductModel();
        modelService.save(productModel);

        List<ProductModel> allOfflineByDateRange = productDao.findAllOfflineByDays(DAYS);

        assertEquals(1, allOfflineByDateRange.size());
    }

    private ProductModel getProductModel() {
        ProductModel productModel = new ProductModel();

        productModel.setCode(PRODUCT_CODE);
        productModel.setCatalogVersion(catalogVersionModel);
        productModel.setOfflineDate(getOfflineDate(OFFLINE_DATE_FROM_NOW));

        return productModel;
    }

    private Date getOfflineDate(Integer days) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, days);

        return calendar.getTime();
    }
}
