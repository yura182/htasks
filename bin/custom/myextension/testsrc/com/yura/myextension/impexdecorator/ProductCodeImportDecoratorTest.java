package com.yura.myextension.impexdecorator;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class ProductCodeImportDecoratorTest {
    private static final int POSITION = 1;
    private static final String TEST_STRING = "Super Product";
    private static final String EXPECTED_STRING = "superproduct";

    @Test
    public void shouldReturnLowercaseStringWithoutSpace() {
        ProductCodeImportDecorator decorator = new ProductCodeImportDecorator();
        Map<Integer, String> srcLine = new HashMap<>();
        srcLine.put(POSITION, TEST_STRING);

        String actual = decorator.decorate(POSITION, srcLine);

        Assert.assertEquals(EXPECTED_STRING, actual);
    }
}
