package com.yura.myextension.service.impl;

import com.yura.myextension.dao.ProductDao;
import de.hybris.platform.core.model.product.ProductModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DefaultProductServiceImplUnitTest {

    public static final int DAYS = 2;
    @Mock
    private ProductDao productDao;

    @Mock
    private ProductModel productModel;

    @InjectMocks
    private DefaultProductServiceImpl productService;

    @Test
    public void shouldReturnListOfProducts() {
        List<ProductModel> expected = Collections.singletonList(productModel);
        when(productDao.findAllOfflineByDays(DAYS)).thenReturn(expected);

        List<ProductModel> actual = productService.getProductsThatBecomeOffline(DAYS);

        assertEquals(expected, actual);
    }
}
