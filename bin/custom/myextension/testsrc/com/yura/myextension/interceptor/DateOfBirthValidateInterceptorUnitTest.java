package com.yura.myextension.interceptor;

import com.yura.myextension.util.LocalDateWrapper;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import org.apache.commons.configuration.Configuration;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DateOfBirthValidateInterceptorUnitTest {

    private static final String DATE_FORMAT = "dd/MM/yyyy";
    private static final String NOT_ALLOWED_DATE_OF_BIRTH = "31/12/2008";
    private static final String ALLOWED_AGE_KEY = "user.allowed.age";
    private static final String ALLOWED_AGE_VALUE = "12";
    private static final String EXPECTED_MESSAGE = "User is under " + ALLOWED_AGE_VALUE + " age old";
    private static final String ALLOWED_DATE_OF_BIRTH = "31/12/2007";
    private static final LocalDate CURRENT_MOCK_DATE = LocalDate.of(2020, 8, 1);

    @Mock
    private ConfigurationService configService;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Mock
    private UserModel userModel;

    @Mock
    private InterceptorContext interceptorContext;

    @Mock
    private Configuration configuration;

    @Mock
    private LocalDateWrapper localDateWrapper;

    @InjectMocks
    private DateOfBirthValidateInterceptor interceptor;

    @Test
    public void shouldValidateUserAgeWithoutThrowingInterceptorException() throws InterceptorException, ParseException {
        when(configService.getConfiguration()).thenReturn(configuration);
        when(configuration.getString(ALLOWED_AGE_KEY)).thenReturn(ALLOWED_AGE_VALUE);
        when(localDateWrapper.getCurrentDate()).thenReturn(CURRENT_MOCK_DATE);
        when(userModel.getDateOfBirth()).thenReturn(getDate(ALLOWED_DATE_OF_BIRTH));

        interceptor.onValidate(userModel, interceptorContext);
    }

    @Test
    public void shouldThrowInterceptorException() throws ParseException, InterceptorException {
        exception.expect(InterceptorException.class);
        exception.expectMessage(EXPECTED_MESSAGE);

        when(configService.getConfiguration()).thenReturn(configuration);
        when(configuration.getString(ALLOWED_AGE_KEY)).thenReturn(ALLOWED_AGE_VALUE);
        when(userModel.getDateOfBirth()).thenReturn(getDate(NOT_ALLOWED_DATE_OF_BIRTH));
        when(localDateWrapper.getCurrentDate()).thenReturn(CURRENT_MOCK_DATE);

        interceptor.onValidate(userModel, interceptorContext);
    }

    private Date getDate(String date) throws ParseException {
        return new SimpleDateFormat(DATE_FORMAT).parse(date);
    }
}
