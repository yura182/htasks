package com.yura.myextension.attributehandler;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CustomerDescriptionAttributeHandlerTest {

    private CustomerDescriptionAttributeHandler handler;

    @Mock
    private CustomerModel customerModel;

    @Mock
    private OrderModel orderModel;

    @Before
    public void setup() {
        handler = new CustomerDescriptionAttributeHandler();
    }

    @Test
    public void get_shouldReturnCustomerDescriptionForOneOrder() {
        mockNameAndEmail();
        when(customerModel.getOrders()).thenReturn(Collections.singletonList(orderModel));

        String expected = "Tom: tom@mail.com. Order quantity is 1";
        String actual = handler.get(customerModel);

        assertEquals(expected, actual);
    }

    @Test
    public void get_shouldReturnCustomerDescriptionForNullOrder() {
        mockNameAndEmail();
        when(customerModel.getOrders()).thenReturn(null);

        String expected = "Tom: tom@mail.com. Order quantity is 0";
        String actual = handler.get(customerModel);

        assertEquals(expected, actual);
    }

    private void mockNameAndEmail() {
        when(customerModel.getName()).thenReturn("Tom");
        when(customerModel.getEmail()).thenReturn("tom@mail.com");
    }
}
