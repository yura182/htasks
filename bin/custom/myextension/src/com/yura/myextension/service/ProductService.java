package com.yura.myextension.service;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

public interface ProductService {

    List<ProductModel> getProductsThatBecomeOffline(Integer days);
}
