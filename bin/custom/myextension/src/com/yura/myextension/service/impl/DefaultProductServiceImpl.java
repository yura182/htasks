package com.yura.myextension.service.impl;

import com.yura.myextension.dao.ProductDao;
import com.yura.myextension.service.ProductService;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

public class DefaultProductServiceImpl implements ProductService {

    private ProductDao productDao;

    public DefaultProductServiceImpl(ProductDao productDao) {
        this.productDao = productDao;
    }

    @Override
    public List<ProductModel> getProductsThatBecomeOffline(Integer days) {
        return productDao.findAllOfflineByDays(days);
    }
}
