package com.yura.myextension.attributehandler;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;

import java.util.Collection;
import java.util.Objects;

public class CustomerDescriptionAttributeHandler extends AbstractDynamicAttributeHandler<String, CustomerModel> {
    private static final String COLON_DELIMITER = ": ";
    private static final String DOT_DELIMITER = ". ";
    private static final String ORDER_SIZE_DESCRIPTION = "Order quantity is ";

    @Override
    public String get(CustomerModel model) {
        return getDescription(model.getName(), model.getEmail(), getOrdersSize(model.getOrders()));
    }

    private String getDescription(String name, String email, int size) {
        return new StringBuilder()
                .append(name)
                .append(COLON_DELIMITER)
                .append(email)
                .append(DOT_DELIMITER)
                .append(ORDER_SIZE_DESCRIPTION)
                .append(size)
                .toString();
    }

    private int getOrdersSize(Collection<OrderModel> orders) {
        return Objects.isNull(orders) ? 0 : orders.size();
    }
}
