package com.yura.myextension.impexdecorator;

import de.hybris.platform.util.CSVCellDecorator;

import java.util.Map;

public class ProductCodeImportDecorator implements CSVCellDecorator {

    @Override
    public String decorate(int position, Map<Integer, String> srcLine) {
        String value = srcLine.get(position);

        return value.toLowerCase().replaceAll(" ", "");
    }
}
