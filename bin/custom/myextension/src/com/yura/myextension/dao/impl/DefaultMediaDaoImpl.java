package com.yura.myextension.dao.impl;

import com.yura.myextension.dao.MediaDao;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;

@Component("defaultMediaDaoImpl")
public class DefaultMediaDaoImpl implements MediaDao {
    private static final String CODE_KEY = "code";
    private static final String CATALOG_VERSION_KEY = "catalogVersion";
    private final static String SELECT_DESCRIPTION_QUERY = "SELECT {m."
            + MediaModel.DESCRIPTION + "} FROM {" + ProductModel._TYPECODE + " AS p JOIN "
            + MediaModel._TYPECODE + " AS m ON {p." + ProductModel.PICTURE + "} = {m." + MediaModel.PK
            + "}} WHERE {p." +  ProductModel.CODE + "} = ?" + CODE_KEY + " AND {m." + MediaModel.CATALOGVERSION
            + "} = ?" + CATALOG_VERSION_KEY + "";

    private FlexibleSearchService flexibleSearchService;

    @Autowired
    public DefaultMediaDaoImpl(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    @Override
    public String findDescriptionByProductCodeAndCatalogVersion(String code, CatalogVersionModel catalogVersionModel) {
        FlexibleSearchQuery searchQuery = new FlexibleSearchQuery(SELECT_DESCRIPTION_QUERY);

        searchQuery.addQueryParameter(CODE_KEY, code);
        searchQuery.addQueryParameter(CATALOG_VERSION_KEY, catalogVersionModel);
        searchQuery.setResultClassList(Collections.singletonList(String.class));

        return flexibleSearchService.searchUnique(searchQuery);
    }
}
