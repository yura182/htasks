package com.yura.myextension.dao;

import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Date;
import java.util.List;

public interface ProductDao {

    ProductModel findByCodeAndCatalogVersion(String code, CatalogVersionModel version);

    List<ProductModel> findAllOfflineByDays(Integer days);
}
