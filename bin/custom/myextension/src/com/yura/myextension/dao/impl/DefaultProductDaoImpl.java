package com.yura.myextension.dao.impl;

import com.yura.myextension.dao.ProductDao;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("defaultProductDaoImpl")
public class DefaultProductDaoImpl implements ProductDao {

    private static final String SELECT_OFFLINE_QUERY = "SELECT {p.pk} FROM {Product AS p} " +
            "WHERE DATEDIFF({p.offlinedate}, CURDATE()) <= ?days";
    private static final String DAYS_KEY = "days";

    private FlexibleSearchService flexibleSearchService;

    @Autowired
    public DefaultProductDaoImpl(FlexibleSearchService flexibleSearchService) {
        this.flexibleSearchService = flexibleSearchService;
    }

    @Override
    public ProductModel findByCodeAndCatalogVersion(String code, CatalogVersionModel version) {
        ProductModel productExample = new ProductModel();

        productExample.setCode(code);
        productExample.setCatalogVersion(version);

        return flexibleSearchService.getModelByExample(productExample);
    }

    @Override
    public List<ProductModel> findAllOfflineByDays(Integer days) {
        FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(SELECT_OFFLINE_QUERY);
        flexibleSearchQuery.addQueryParameter(DAYS_KEY, days);

        SearchResult<ProductModel> searchResult = flexibleSearchService.search(flexibleSearchQuery);

        return searchResult.getResult();
    }
}
