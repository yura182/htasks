package com.yura.myextension.dao;

import de.hybris.platform.catalog.model.CatalogVersionModel;

public interface MediaDao {

    String findDescriptionByProductCodeAndCatalogVersion(String code, CatalogVersionModel catalogVersionModel);
}
