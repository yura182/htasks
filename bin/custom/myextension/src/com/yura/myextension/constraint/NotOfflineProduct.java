package com.yura.myextension.constraint;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = NotOfflineProductValidator.class)
@Documented
public @interface NotOfflineProduct {
    String message() default "{com.yura.myextension.constraint.NotOfflineProduct.message}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
