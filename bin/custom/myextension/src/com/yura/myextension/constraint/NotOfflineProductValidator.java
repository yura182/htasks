package com.yura.myextension.constraint;

import com.yura.myextension.model.ProductBundleModel;
import de.hybris.platform.core.model.product.ProductModel;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Date;

public class NotOfflineProductValidator implements ConstraintValidator<NotOfflineProduct, ProductBundleModel> {
    @Override
    public void initialize(NotOfflineProduct notOfflineProduct) {

    }

    @Override
    public boolean isValid(ProductBundleModel productBundleModel, ConstraintValidatorContext constraintValidatorContext) {
        return productBundleModel.getProducts() != null &&
                productBundleModel.getProducts()
                .stream()
                .noneMatch(this::isOffline);

    }

    private boolean isOffline(ProductModel productModel) {
        Date today = new Date();
        return (productModel.getOnlineDate() != null && today.before(productModel.getOnlineDate())) ||
                (productModel.getOfflineDate() != null && today.after(productModel.getOfflineDate()));
    }

}
