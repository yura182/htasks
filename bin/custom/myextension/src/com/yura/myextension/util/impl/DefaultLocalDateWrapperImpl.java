package com.yura.myextension.util.impl;

import com.yura.myextension.util.LocalDateWrapper;

import java.time.LocalDate;

public class DefaultLocalDateWrapperImpl implements LocalDateWrapper {

    @Override
    public LocalDate getCurrentDate() {
        return LocalDate.now();
    }
}
