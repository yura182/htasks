package com.yura.myextension.util;

import java.time.LocalDate;

public interface LocalDateWrapper {

    LocalDate getCurrentDate();
}
