package com.yura.myextension.interceptor;

import com.yura.myextension.util.LocalDateWrapper;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;

public class DateOfBirthValidateInterceptor implements ValidateInterceptor<UserModel> {
    private static final String ALLOWED_AGE = "user.allowed.age";

    private ConfigurationService configService;
    private LocalDateWrapper localDateWrapper;

    public DateOfBirthValidateInterceptor(ConfigurationService configService, LocalDateWrapper localDateWrapper) {
        this.configService = configService;
        this.localDateWrapper = localDateWrapper;
    }

    @Override
    public void onValidate(UserModel userModel, InterceptorContext interceptorContext) throws InterceptorException {
        int age = getAge(userModel);
        int allowedAge = getAllowedAge();

        if (age < allowedAge) {
            throw new InterceptorException("User is under " + allowedAge + " age old");
        }
    }

    private int getAge(UserModel userModel) {
        LocalDate today = localDateWrapper.getCurrentDate();
        LocalDate dateOfBirth = userModel.getDateOfBirth().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        return Period.between(dateOfBirth, today).getYears();
    }

    private int getAllowedAge() {
        return Integer.parseInt(configService.getConfiguration().getString(ALLOWED_AGE));
    }
}
