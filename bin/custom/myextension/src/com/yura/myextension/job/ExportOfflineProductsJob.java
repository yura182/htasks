package com.yura.myextension.job;

import com.yura.myextension.service.ProductService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.util.CSVWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ExportOfflineProductsJob extends AbstractJobPerformable<CronJobModel> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExportOfflineProductsJob.class);

    private static final String FILE_PATH_KEY = "myextension.job.export.offline.file.path";
    private static final String DAYS_KEY = "myextension.job.export.offline.days";

    private ProductService productService;
    private ConfigurationService configurationService;

    public ExportOfflineProductsJob(ProductService productService, ConfigurationService configurationService) {
        this.productService = productService;
        this.configurationService = configurationService;
    }

    @Override
    public PerformResult perform(CronJobModel cronJobModel) {
        List<ProductModel> productModels = productService.getProductsThatBecomeOffline(getIntProperty(DAYS_KEY));

        try (FileWriter fileWriter = new FileWriter(getProperty(FILE_PATH_KEY));
             CSVWriter csvWriter = new CSVWriter(fileWriter)) {

            csvWriter.write(getMapOfCodes(productModels));
        } catch (IOException e) {
            LOGGER.warn(e.getMessage(), e);
            return new PerformResult(CronJobResult.FAILURE, CronJobStatus.FINISHED);
        }

        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }

    private Map<Integer, String> getMapOfCodes(List<ProductModel> productModels) {
        return IntStream.range(0, productModels.size())
                .boxed()
                .collect(Collectors.toMap(k -> k, k -> productModels.get(k).getCode()));
    }

    private int getIntProperty(String key) {
        return Integer.parseInt(getProperty(key));
    }

    private String getProperty(String key) {
        return configurationService.getConfiguration().getString(key);
    }
}
